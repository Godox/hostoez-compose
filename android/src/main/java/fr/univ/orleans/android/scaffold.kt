package fr.univ.orleans.android

import androidx.compose.foundation.layout.Column
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable
fun MyScaffold() {
    var isDrawerOpen by remember { mutableStateOf(false) }
    val scaffoldState = rememberScaffoldState()

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = { Text(text = "My App") },
                navigationIcon = {
                    IconButton(onClick = { isDrawerOpen = true }) {
                        Icon(Icons.Filled.Menu, contentDescription = "Menu")
                    }
                }
            )
        },
        drawerContent = {
            Column {
                Text(text = "Drawer Content")
                Divider(color = Color.Gray, thickness = 1.dp)
                Text(text = "Item 1")
                Text(text = "Item 2")
                Text(text = "Item 3")
            }
        },
        content = {
            Column {
            }
        },
        bottomBar = {
            BottomAppBar(
                backgroundColor = Color.White,
                elevation = 8.dp
            ) {
                IconButton(
                    onClick = { /* Do something */ },
                    modifier = androidx.compose.ui.Modifier.weight(1f)
                ) {
                    Icon(
                        // Deplace le fichier dans le module android

                        painter = painterResource(id = R.drawable.ic_home),
                        contentDescription = "Home"
                    )
                }
                IconButton(
                    onClick = { /* Do something */ },
                    modifier = androidx.compose.ui.Modifier.weight(1f)
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_search),
                        contentDescription = "Search"
                    )
                }
                IconButton(
                    onClick = { /* Do something */ },
                    modifier = androidx.compose.ui.Modifier.weight(1f)
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_settings),
                        contentDescription = "Settings"
                    )
                }
            }
        }
    )
}
