package fr.univ.orleans.common

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Person
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

class Conversation(
    val conversationId: Int = -1,
    val conversationName: String = "Conversation",
    val messages: SnapshotStateList<Message> = SnapshotStateList()
) {
    init {
        messages.addAll(
            listOf(
                Message("Salut", false),
                Message("Mon", false),
                Message("Pote", false),
            )
        )
    }


    fun addMessage(message: String) {
        messages.add(Message(message))
    }
}

class Message(val content: String, val self: Boolean = true)


@Composable
fun MessageView() {
    val conversations = remember {
        mutableStateListOf(
            Conversation(1, "André"),
            Conversation(2, "JeanMi"),
            Conversation(3, "Fred")
        )
    }
    val selectedConversation = remember { mutableStateOf<Conversation?>(null) }
    Row {
        NavigationRail(header = { Row(Modifier.background(Color.Green)) { Text("Messages") } }) {
            conversations.forEach { item ->
                NavigationRailItem(
                    label = { Text(item.conversationName) },
                    icon = { Icon(Icons.Outlined.Person, contentDescription = item.conversationName) },
                    selected = selectedConversation.value == item,
                    onClick = {
                        selectedConversation.value = item
                    },
                    alwaysShowLabel = true
                )
            }
        }

        selectedConversation.value?.let {
            Column {
                TopAppBar {
                    Row {
                        Text(it.conversationName)
                    }
                }
                Conversation(it, it.messages)
            }
        }
    }
}

