package fr.univ.orleans.common.api.auth

import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.client.statement.HttpResponse
import io.ktor.http.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put
import kotlin.coroutines.CoroutineContext


object AuthenticationApi : CoroutineScope {

    private val clientJob = SupervisorJob()

    var AUTHORIZATION = MutableStateFlow("")

    lateinit var CURRENT_ACCOUNT : MutableStateFlow<AccountDTO>

    val errorsFlow = MutableStateFlow<HostoezError?>(null)

    val uriBuilder = URLBuilder("http://localhost:8088/api/authent/")

    override val coroutineContext: CoroutineContext = Dispatchers.IO + clientJob

    val httpClient = HttpClient {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    @Serializable
    data class AccountDTO(val id: Long, val email: String, val password: String, val role: String, val authorities: List<String> = listOf()) {
        lateinit var token: String
    }

    fun register(email: String, password: String, callback: (AccountDTO) -> Unit = {}) {
        launch {
            val response = httpClient.post<HttpResponse>("http://localhost:8088/api/authent/inscription") {
                contentType(ContentType.Application.Json)
                body = buildJsonObject {
                    put("email", email)
                    put("password", password)
                    put("role", "ADMIN")
                }
            }
            if (response.status == HttpStatusCode.Unauthorized) {
                errorsFlow.update { HostoezError(response.readText()) }
            }
            val responseBody = Json.decodeFromString<AccountDTO>(response.readText())
            response.headers["Authorization"]?.let {
                responseBody.token = it
            }
            callback(responseBody)
        }
    }

    fun login(email: String, password: String, callback: (AccountDTO) -> Unit = {}) {
        launch {
            val response = httpClient.post<HttpResponse>("http://localhost:8088/api/authent/login") {
                contentType(ContentType.Application.Json)
                body = buildJsonObject {
                    put("email", email)
                    put("password", password)
                }
            }
            val responseBody = Json.decodeFromString<AccountDTO>(response.readText())
            response.headers["Authorization"]?.let {
                responseBody.token = it
            }
            callback(responseBody)
        }
    }



}

class HostoezError(val message: String)
