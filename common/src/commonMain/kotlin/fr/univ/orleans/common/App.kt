package fr.univ.orleans.common

import androidx.compose.foundation.layout.Row
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.vector.ImageVector

@Composable
fun App() {

    var text by remember { mutableStateOf("Hello, World!") }
    val platformName = getPlatformName()

    MainNavigationRail()
}

val icons = listOf(Icons.Filled.Home, Icons.Filled.Search, Icons.Filled.Settings)

enum class Page(val title: String, val icon: ImageVector, val content: @Composable () -> Unit) {
    PROFILE("Profile", Icons.Filled.Person, { ProfileView() }),
    LOGIN("Login", Icons.Filled.ExitToApp, { LoginView() }),
    HOME("Home", Icons.Filled.Home, { HomeView() }),
    MESSAGE("Messages", Icons.Filled.Email, { MessageView() }),
    DOCUMENTS("Documents", Icons.Filled.Info, { DocumentView() })
}

val selectedMainTab = mutableStateOf(Page.LOGIN)

@Composable
fun MainNavigationRail() {
    val pages = Page.values()
    Row {
        NavigationRail {
            pages.forEachIndexed { index, item ->
                PageNavItem(item, selectedMainTab)
            }
        }
        selectedMainTab.value.content()
    }
}


@Composable
fun PageNavItem(item: Page, selectedItem: MutableState<Page>) {
    NavigationRailItem(
        label = { Text(item.title) },
        icon = { Icon(item.icon, contentDescription = item.title) },
        selected = selectedItem.value == item,
        onClick = { selectedItem.value = item },
        alwaysShowLabel = true
    )
}
