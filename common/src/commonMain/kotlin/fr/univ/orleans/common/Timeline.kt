package fr.univ.orleans.common

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp


val MARGIN_SCROLLBAR: Dp = 8.dp

@Composable
fun Timeline(
    room: Conversation,
    items: List<Message>,
    modifier: Modifier = Modifier,
    scrollbar: @Composable BoxScope.(LazyListState) -> Unit = {}
) {
    Box(
        modifier = modifier.fillMaxHeight(0.8F),
        contentAlignment = Alignment.BottomEnd
    ) {
        val listState = rememberLazyListState()

        LazyColumn(
            state = listState,
            modifier = Modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(5.dp)
        ) {
            items(items) { message ->
                TimelineElement(message)
            }
        }
        scrollbar(listState)
    }
}

@Composable
private fun TimelineElement(
    message: Message,
    modifier: Modifier = Modifier
) {

    val hArrangement = if (message.self) Arrangement.End else Arrangement.Start

    Row(
        modifier = modifier.fillMaxWidth(),
        horizontalArrangement = hArrangement
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(0.7F),
            horizontalArrangement = hArrangement
        ) {
            Spacer(modifier = Modifier.width(8.dp))
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .background(
                        color = if (message.self) Color(0, 135, 255) else Color(76, 78, 82),
                        shape = RoundedCornerShape(corner = CornerSize(8.dp))
                    )
            ) {
                Row(
                    modifier = Modifier
                        .align(Alignment.Center)
                        .padding(5.dp)
                ) {
                    SelectionContainer {
                        Text(
                            text = AnnotatedString(message.content),
                            overflow = TextOverflow.Ellipsis,
                            color = Color.White
                        )
                    }
                }
            }
        }
        Spacer(modifier = Modifier.width(MARGIN_SCROLLBAR))
    }
}
