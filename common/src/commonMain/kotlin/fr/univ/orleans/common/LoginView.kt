package fr.univ.orleans.common

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import fr.univ.orleans.common.api.auth.AuthenticationApi

@OptIn(ExperimentalUnitApi::class)
@Composable
fun LoginView() {
    val email = remember { mutableStateOf("") }
    val password = remember { mutableStateOf("") }
    val passwordVisible = remember { mutableStateOf(false) }
    val register = remember { mutableStateOf(false) }

    Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(20.dp)
        ) {
            Text(text = if (register.value) "Inscription" else "Connexion", fontSize = TextUnit(3f, TextUnitType.Em))
            // deux colones avec un texteet un textfield
            // une pour le login et une pour le mot de passe
            Column {
                Text(text = "Email")
                TextField(value = email.value, singleLine = true, onValueChange = { email.value = it })
            }
            Column {
                Text(text = "Password")
                TextField(value = password.value,
                    singleLine = true,
                    visualTransformation = if (passwordVisible.value) VisualTransformation.None else PasswordVisualTransformation(),
                    trailingIcon = {
                        IconButton({ passwordVisible.value = passwordVisible.value.not() }) {
                            Icon(
                                if (passwordVisible.value) Icons.Filled.VisibilityOff else Icons.Filled.Visibility,
                                "visibility"
                            )
                        }
                    },
                    onValueChange = { password.value = it })
                AuthenticationApi.errorsFlow.collectAsState().value?.also { error ->
                    Text(
                        error.message,
                        color = Color.Red
                    )
                }
            }
            if (register.value) {
                Text("Name")
            }
            Row(
                horizontalArrangement = Arrangement.spacedBy(20.dp, Alignment.CenterHorizontally),
            ) {
                Button(onClick = {
                    if (!register.value) {
                        AuthenticationApi.login(email.value, password.value) {
                            selectedMainTab.value = Page.HOME
                        }
                    } else register.value = register.value.not()
                }
                ) {
                    Text("Login")
                }
                Button(onClick = {
                    if (register.value) {
                        AuthenticationApi.register(email.value, password.value) {
                            selectedMainTab.value = Page.PROFILE
                        }
                    } else register.value = register.value.not()
                }) {
                    Text("Register")
                }
            }
        }
    }
}

@Preview
@Composable
fun preview() = LoginView()
