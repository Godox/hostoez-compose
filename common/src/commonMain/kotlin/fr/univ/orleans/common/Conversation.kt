package fr.univ.orleans.common

import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.isShiftPressed
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.unit.dp


@Composable
fun Conversation(
    currentRoom: Conversation,
    currentTimeline: MutableList<Message>
) {
    Column {
        Timeline(
            currentRoom,
            currentTimeline,
            modifier = Modifier.weight(1f),
            scrollbar = {
                VerticalScrollbar(
                    modifier = Modifier.align(Alignment.CenterEnd),
                    adapter = rememberScrollbarAdapter(scrollState = it)
                )
            }
        )
        MessageField(currentRoom)
    }

}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun MessageField(currentRoom: Conversation) {

    var message by remember { mutableStateOf("") }

    TextField(
        value = message,
        onValueChange = {
            message = it
        },
        placeholder = {
            Text(
                text = "Enter your message",
                color = Color.Gray
            )
        },
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)
            .onKeyEvent {
                when {
                    (it.key == Key.Enter && message.isNotEmpty() && message.isNotBlank()) -> {
                        if (!it.isShiftPressed) {
                            currentRoom.addMessage(message.substring(0, message.length - 1))
                            message = ""
                            true
                        } else true
                    }
                    else -> false
                }
            },
        trailingIcon = {
            IconButton(
                onClick = {
                    if (message.isNotEmpty() && message.isNotBlank()) {
                        currentRoom.addMessage(message)
                        message = ""
                    }
                },
            ) {
                Icon(Icons.Filled.Send, "Send")
            }
        }
    )
}
