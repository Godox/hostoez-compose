import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import fr.univ.orleans.common.App


fun main() = application {
    Window(title = "Hostoez", onCloseRequest = ::exitApplication) {
        App()
    }
}
